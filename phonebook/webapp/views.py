from django.shortcuts import render, redirect
from django.db.models import Q
from django.core.paginator import Paginator

from .forms import NewContactForm, NewPhoneForm, NewEmailForm
from .forms import NewGroupForm, NewOfficeForm, NewLocationForm

from .models import Contact, Group, Office, Location, Settings


def listContacts(request, contact_id=False):
    config = Settings.objects.get(active=True)

    if request.method == "POST":
        criteria = request.POST['criteria']
        contact_list = Contact.objects.filter(
                    Q(first_name__contains=criteria) |
                    Q(middle_name__contains=criteria) |
                    Q(last_name__contains=criteria) |
                    Q(initials__contains=criteria)
        )
    else:
        contact_list = Contact.objects.all()

    page = request.GET.get('page')
    paginator = Paginator(contact_list, config.max_item_per_list)
    contacts = paginator.get_page(page)

    context = {'contacts': contacts, 'can_add': config.add_contact}

    if contact_id:
        context['contact_to_show'] = Contact.objects.get(pk=contact_id)

    return render(request, 'webapp/contact_list.html', context)


def new_contact(request):
    if request.method == "POST":
        form = NewContactForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('webapp:contact_detail', contact_id=post.pk)
    else:
        form = NewContactForm()
        return render(
                    request,
                    'webapp/add_form.html',
                    {'form': form, 'title': "Add Contact"}
        )
    return ""  # This one will never be reached!


def new_contact_phone(request, contact_id):
    if request.method == "POST":
        form = NewPhoneForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('webapp:contact_detail', contact_id=post.contact_id)
    else:
        form = NewPhoneForm(initial={'contact': contact_id})
        return render(
                    request,
                    'webapp/add_form.html',
                    {'form': form, 'title': "Add Phone"}
        )
    return ""  # This one will never be reached!


def new_contact_email(request, contact_id):
    if request.method == "POST":
        form = NewEmailForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('webapp:contact_detail', contact_id=post.contact_id)
    else:
        form = NewEmailForm(initial={'contact': contact_id})
        return render(
                    request,
                    'webapp/add_form.html',
                    {'form': form, 'title': "Add Email"}
        )
    return ""  # This one will never be reached!


def groups(request, group_id=False):
    config = Settings.objects.get(active=True)
    group_list = Group.objects.all()

    page = request.GET.get('page')
    paginator = Paginator(group_list, config.max_item_per_list)
    groups = paginator.get_page(page)
    context = {'groups': groups, 'can_add': config.add_group}

    if group_id:
        context['group_to_show'] = Group.objects.get(pk=group_id)

    return render(request, 'webapp/group_list.html', context)


def new_group(request):
    if request.method == "POST":
        form = NewGroupForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('webapp:group_detail', group_id=post.pk)
    else:
        form = NewGroupForm()
        return render(
                    request,
                    'webapp/add_form.html',
                    {'form': form, 'title': "Add Group"}
        )
    return ""  # This will bring errors


def offices(request, office_id=False):
    config = Settings.objects.get(active=True)
    office_list = Office.objects.all()
    page = request.GET.get('page')
    paginator = Paginator(office_list, config.max_item_per_list)
    offices = paginator.get_page(page)
    context = {'offices': offices, 'can_add': config.add_office}

    if office_id:
        context['office_to_show'] = Office.objects.get(pk=office_id)

    return render(request, 'webapp/office_list.html', context)


def new_office(request):
    if request.method == "POST":
        form = NewOfficeForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('webapp:office_detail', office_id=post.pk)
    else:
        form = NewOfficeForm()
        return render(
                    request,
                    'webapp/add_form.html',
                    {'form': form, 'title': "Add Office"}
        )
    return ""  # This will bring errors


def locations(request, location_id=False):
    config = Settings.objects.get(active=True)
    location_list = Location.objects.all()
    page = request.GET.get('page')
    paginator = Paginator(location_list, config.max_item_per_list)
    locations = paginator.get_page(page)
    context = {'locations': locations, 'can_add': config.add_location}

    if location_id:
        context['location_to_show'] = Location.objects.get(pk=location_id)

    return render(request, 'webapp/location_list.html', context)


def new_location(request):
    if request.method == "POST":
        form = NewLocationForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('webapp:location_detail', location_id=post.pk)
    else:
        form = NewLocationForm()
        return render(
                    request,
                    'webapp/add_form.html',
                    {'form': form, 'title': "Add Location"}
        )
    return ""  # This will bring errors
