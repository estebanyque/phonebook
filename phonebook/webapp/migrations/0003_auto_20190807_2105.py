# Generated by Django 2.2.3 on 2019-08-07 21:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0002_configuration'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Configuration',
            new_name='Settings',
        ),
    ]
