# Generated by Django 2.2.3 on 2019-08-07 21:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0003_auto_20190807_2105'),
    ]

    operations = [
        migrations.RenameField(
            model_name='settings',
            old_name='can_add_contact',
            new_name='add_contact',
        ),
        migrations.RenameField(
            model_name='settings',
            old_name='can_add_group',
            new_name='add_group',
        ),
        migrations.RenameField(
            model_name='settings',
            old_name='can_add_office',
            new_name='add_location',
        ),
        migrations.AddField(
            model_name='settings',
            name='add_office',
            field=models.BooleanField(default=True),
        ),
    ]
