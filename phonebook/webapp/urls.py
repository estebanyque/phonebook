from django.urls import path

from . import views

app_name = "webapp"
urlpatterns = [
    # path('', views.index, name='index'),
    path('', views.listContacts, name='index'),
    path('<int:contact_id>', views.listContacts, name='contact_detail'),
    path('groups/', views.groups, name='groups'),
    path('groups/<int:group_id>', views.groups, name='group_detail'),
    path('office/', views.offices, name='offices'),
    path('office/<int:office_id>', views.offices, name='office_detail'),
    path('location/', views.locations, name='locations'),
    path('location/<int:location_id>', views.locations, name='location_detail'),
    path('new/contact', views.new_contact, name='new_contact'),
    path('new/contact/phone/<int:contact_id>', views.new_contact_phone, name='new_contact_phone'),
    path('new/contact/email/<int:contact_id>', views.new_contact_email, name='new_contact_email'),
    path('new/group', views.new_group, name="new_group"),
    path('new/office', views.new_office, name="new_office"),
    path('new/location', views.new_location, name="new_location"),

]
