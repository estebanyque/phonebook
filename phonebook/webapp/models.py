from django.db import models
from fontawesome_5.fields import IconField

# from django.urls import reverse
# Create your models here.

MAX_ENTRIES = 2

class Settings(models.Model):
    max_entries = models.IntegerField(default=2)
    add_entry = models.BooleanField(default=True)
    add_contact = models.BooleanField(default=True)
    add_group = models.BooleanField(default=True)
    add_office = models.BooleanField(default=True)
    add_location = models.BooleanField(default=True)
    max_item_per_list = models.IntegerField(default=10)
    google_map_link = models.BooleanField(default=True)
    active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.created_at) + " - " + str(self.active)

    def can_add_location(self):
        return self.add_location

class Location(models.Model):
    name = models.CharField(max_length=256)
    address = models.TextField()
    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

class Office(models.Model):
    name = models.CharField(max_length=256)
    alias = models.CharField(max_length=64)
    deleted = models.BooleanField(default=False)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

class Group(models.Model):
    name = models.CharField(max_length=256)
    alias = models.CharField(max_length=64)
    deleted = models.BooleanField(default=False)
    office = models.ForeignKey(Office, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

class Contact(models.Model):
    first_name = models.CharField(max_length=128)
    middle_name = models.CharField(max_length=128, null=True, blank=True)
    last_name = models.CharField(max_length=128)
    initials = models.CharField(max_length=4, null=True, blank=True)
    deleted = models.BooleanField(default=False)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    fa = IconField(default="fas,user")  # Fontawesome Icon
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('last_name', 'first_name', 'middle_name')

    def __str__(self):
        return self.name()

    def get_setting(self):
        setting = Settings.objects.get(active=True)
        return setting

    def name(self):
        if not self.middle_name:
            middle_name = " "
        else:
            middle_name = self.middle_name
        name = "{} {} {}".format(self.first_name, middle_name, self.last_name)
        return name

    def getPhones(self):
        return Phone.objects.filter(contact=self.pk).order_by('type')

    def getEmails(self):
        return Email.objects.filter(contact=self.pk).order_by('type')

    def actions_available(self):

        return ""

    def can_add_email(self):
        setting = self.get_setting()
        if setting.add_entry:
            if len(self.getEmails()) < MAX_ENTRIES:
                return True

        return False

    def can_add_phone(self):
        setting = self.get_setting()
        if setting.add_entry:
            if len(self.getPhones()) < MAX_ENTRIES:
                return True

        return False

class TypeContact(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=512)
    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Email(models.Model):
    account = models.CharField(max_length=256)
    domain = models.CharField(max_length=128, null=True, blank=True)
    deleted = models.BooleanField(default=False)
    type = models.ForeignKey(TypeContact, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.account

class Phone(models.Model):
    country_code = models.IntegerField(default=56)
    number = models.CharField(max_length=16)
    annex = models.IntegerField(null=True, blank=True)
    deleted = models.BooleanField(default=False)
    type = models.ForeignKey(TypeContact, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        phone_number = "+" + str(self.country_code) + " " + str(self.number)
        return self.contact.name() + " " + phone_number

    def get_country_code(self):
        cc = "+" + str(self.country_code)
        return cc

    def full_number(self):
        if self.number.startswith(self.get_country_code()):
            phone_number = self.number
        else:
            phone_number = self.get_country_code() + " " + str(self.number)
        if self.annex :
            phone_number += " Anexo "+ str(self.annex)
        return phone_number
