from django import forms
from .models import Contact, Phone, Email, Group, Office, Location

class NewContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ('first_name',
                'middle_name',
                'last_name',
                'initials',
                'group',
        )


class NewPhoneForm(forms.ModelForm):
    class Meta:
        model = Phone
        fields = ('country_code', 'number', 'annex', 'type', 'contact')
        widgets = {'contact': forms.HiddenInput()}


class NewEmailForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = ('account', 'type', 'contact')
        widgets = {'account': forms.EmailInput(), 'contact': forms.HiddenInput()}


class NewGroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ('name', 'alias', 'office')


class NewOfficeForm(forms.ModelForm):
    class Meta:
        model = Office
        fields = ('name', 'alias', 'location')


class NewLocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = ('name', 'address')
