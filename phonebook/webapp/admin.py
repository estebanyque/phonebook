from django.contrib import admin
from .models import Location, Office, Group
from .models import TypeContact, Email, Phone
from .models import Contact
from .models import Settings
# Register your models here.

admin.site.register(Settings)

admin.site.register(Contact)
admin.site.register(Group)
admin.site.register(Office)
admin.site.register(Location)
admin.site.register(TypeContact)

admin.site.register(Email)
admin.site.register(Phone)
