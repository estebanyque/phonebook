# README #

Hi!

This is a simple phonebook webapp created using Django.

### What is this repository for? ###

* Phonebook
* Version 0.1

### How do I get set up? ###

To use this app, read the next steps and you will be ready to go:

* Python and Django setup

  python -m venv env
  source env/bin/activate
  pip install --upgrade pip
  pip install django

* Django Initial configuration
  django-admin startproject phonebook
  cd phonebook
  python manage.py startapp webapp
  python manage.py makemigrations
  python manage.py migrate
  python manage.py createsuperuser
    admin/password (you should change this)

* PIP Dependencies
Read the file phonebook/requirements.txt
But if you're in a hurry
```
pip install phonebook/requirements.txt
```

### Contribution guidelines ###

The main goal of this app, has been achieved, so us it.
Hopefully I will add more changes (check the TODO file)

If you want to contact me, write to:
e [dot] osorio [dot] gallardo [at] protonmail [dot] com

### Do you enjoy it? ###

* If you enjoy it, feel free to send me a buck to bandcamp.com/estebanyque
